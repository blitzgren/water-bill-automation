var phantom = require('phantom');

function waitUntil(asyncTest, timeout) {
    return new Promise(function(resolve, reject) {
        var start = Date.now();
        function wait() {
            console.log("waiting");
            if ((Date.now() - start) >= timeout) {
                reject();
            }
            else {
                asyncTest()
                .then(function(value) {
                    if (value === true) {
                        resolve();
                    }
                    else {
                        setTimeout(wait, 100);
                    }
                }).catch(function(e) {
                    console.log('Error found. Rejecting.', e);
                    reject();
                });
            }
        }
        wait();
    });
}

function waitForVisible(page, selector, timeout) {
    return waitUntil(() => {
        return page.evaluate(function(selector) {
            var elem = document.querySelector(selector);
            return elem && !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
        }, selector)
        .catch(function(e) {
            console.log('Error found. Rejecting.', e);
            reject();
        });
        
    }, timeout);
}

function enterId(page, id, idType) {
    var _page = page;
    return waitForVisible(_page, '#ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_txtAccountNumber', 10000)
    .then(() => {
        return _page.evaluate(function(id, idType) {
            const ACCOUNT_ID_TYPE = "account";
            const ADDRESS_ID_TYPE = "address";

            var element = null;
            var button = null;
            switch (idType) {
                case ACCOUNT_ID_TYPE:
                    element = document.getElementById('ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_txtAccountNumber');
                    button = 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_btnGetInfoAccount';
                    break;
                case ADDRESS_ID_TYPE:
                    element = document.getElementById('ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_ucServiceAddress_txtServiceAddress');
                    button = 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_btnGetInfoServiceAddress';
                    break;
                default:
                    throw Error("No id type found for " + idType);
            }
            element.value = id;
            return button;
        }, id, idType);
    }).then(function(button) {
        console.log("Entered info, found button to click", button);
        return _page.evaluate(function(button) {
            var element = document.getElementById(button);
            var ev = document.createEvent("MouseEvent");
            ev.initMouseEvent(
                "click",
                true /* bubble */, true /* cancelable */,
                window, null,
                0, 0, 0, 0, /* coordinates */
                false, false, false, false, /* modifier keys */
                0 /*left*/, null
            );
            element.dispatchEvent(ev);
        }, button);
    }).then(() => {
        console.log("Waiting for visible bill");
        return waitForVisible(_page, '#ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_pnlBillDetail', 10000);
    }).catch(e => {
        throw Error("Did not find " + idType + " " + id + ": [" + JSON.stringify(e) + "]");
    });
}

const dataPullMap = {
    accountId: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblAccountNumber',
    serviceAddress: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblServiceAddress',
    currentReadDate: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblLastReadDate',
    currentBillDate: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblBillDate',
    penaltyDate: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblPenaltyDate',
    currentBillAmount: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblLastBillAmount',
    previousBalance: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblPreviousBalance',
    currentBalance: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblCurrentBalance',
    previousReadDate: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblPreviousReadDate',
    lastPayDate: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblLastPayDate',
    lastPayAmount: 'ctl00_ctl00_rootMasterContent_LocalContentPlaceHolder_lblLastPayAmount'
};

function crawl(url, id, idType) {
    var _ph, _page, billDetails;
    var result = {
        success: false,
        info: "",
        waterBill: {}
    };

    return phantom.create().then(ph => {
        _ph = ph;
        return _ph.createPage();
    }).then(page => {
        _page = page;
        return _page.open(url);
    }).then(status => {
        console.log("Status: ", status);
        if (status != 'success') {
            throw Error("Could not open page")
        }
        return _page.property('content');
    }).then(content => {
        return enterId(_page, id, idType);
    }).then(() => {
        return _page.evaluate(function(dataNeeded) {
            var out = {};
            for (var key in dataNeeded) {
                out[key] = document.getElementById(dataNeeded[key]).innerHTML.trim();
            }
            return out;
        }, dataPullMap);
    }).then(resultData => {
        console.log("Data recorded:", JSON.stringify(resultData));
        result.waterBill = resultData;
        result.success = true;
    }).then(() => {
         return _page.close();
    }).then(() => {
        _ph.exit();
        return result;
    }).catch(e => {
        console.error("ERROR: " + e);
        _ph.exit();
        result.success = false;
        result.info = e.toString();
        return result;
    });
}

function run(id, idType, context) {
    console.log("Evaluating", idType, id);
    crawl('http://cityservices.baltimorecity.gov/water/', id, idType).then(result => {
        if (context) {
            context.done(null, result);
        }
        else {
            console.log("Result: " + JSON.stringify(result));
        }
    });
}

module.exports = {
    run: run
}