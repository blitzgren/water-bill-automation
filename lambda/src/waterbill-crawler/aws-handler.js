
(function() {
    var childProcess = require("child_process");
    var oldSpawn = childProcess.spawn;
    function mySpawn() {
        console.log('spawn called');
        console.log(arguments);
        var result = oldSpawn.apply(this, arguments);
        return result;
    }
    childProcess.spawn = mySpawn;
})();

var crawler = require('./waterbill-crawler.js');

exports.handler = function (event, context) {
    process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'] 
        + ':' + process.env['LAMBDA_TASK_ROOT'] + '/node_modules/phantomjs-prebuilt/bin';
        // + ':' + process.env['LAMBDA_TASK_ROOT'] + '/node_modules/casperjs/bin';
    console.log( 'PATH: ' + process.env.PATH );
    crawler.run(event.id, event.idType, context);
}