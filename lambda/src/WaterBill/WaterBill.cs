﻿using System;
using Amazon.Lambda.Core;
using Amazon.Lambda.Serialization.Json;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WaterBill
{
    public class Handler
    {

        public Handler()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("config.json");

            Configuration = builder.Build();
        }
        public IConfigurationRoot Configuration { get; set; }
        
        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
        public async Task<WaterBillResponseModel> Get(WaterBillEmailModel input)
        {
            if (input == null || string.IsNullOrWhiteSpace(input.Id))
            {
                return new WaterBillResponseModel
                {
                    Success = false
                };
            }

            var waterbillTableName = Configuration["waterBillTable"];
            var repo = new WaterBillRepository(new AmazonDynamoDBClient(), waterbillTableName);

            var result = await repo.GetWaterBill(input.Id);
            var success = result != null;
            
            return new WaterBillResponseModel
            {
                Model = result,
                Success = success
            };
        }
    }
}
