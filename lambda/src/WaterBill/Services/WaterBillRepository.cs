using System;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using System.Collections.Generic;

namespace WaterBill
{
    public class WaterBillRepository
    {
        private AmazonDynamoDBClient client;
        private string tableName;

        public WaterBillRepository(AmazonDynamoDBClient client, string tableName)
        {
            this.client = client;
            this.tableName = tableName;
        }

        public async Task<WaterBillEmailModel> GetWaterBill(string id)
        {
            var table = GetTable(this.tableName);
            
            var config = new GetItemOperationConfig
            {
                AttributesToGet = new List<string> { "id", "email" },
                ConsistentRead = false
            };

            var document = await table.GetItemAsync(id, config);
            if (document == null) 
            {
                return null;
            }

            return new WaterBillEmailModel
            {
                Id = document["id"],
                Email = document["email"]
            };
        }

        protected Table GetTable(string tableName)
        {
            // Load Dynamo
            var client = new AmazonDynamoDBClient();

            // Retrieve Group table
            var resultTable = Table.LoadTable(client, tableName);
            if (resultTable == null)
            {
                throw new Exception(string.Format("No table found for {0}", tableName));
            }

            return resultTable;
        }
    }
}