namespace WaterBill
{
    public class WaterBillResponseModel 
    {
        public WaterBillEmailModel Model { get; set; }
        public bool Success { get; set; }
    }
}