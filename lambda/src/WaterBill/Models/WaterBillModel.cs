namespace WaterBill
{
    public class WaterBillEmailModel 
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}