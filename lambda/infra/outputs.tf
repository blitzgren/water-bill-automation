output "waterbill_endpoint" {
  value = "https://${aws_api_gateway_rest_api.waterbill_api.id}.execute-api.${var.region}.amazonaws.com/release/${var.release}_waterbill"
}
