variable "name" {
  description = "The name of the lambda to create, which also defines (i) the archive name (.zip), (ii) the file name, and (iii) the function name"
}

variable "runtime" {
  description = "The runtime of the lambda to create"
  default     = "nodejs"
}

variable "handler" {
  description = "The handler name of the lambda (a function defined in your lambda)"
  default     = "handler"
}

variable "role" {
  description = "IAM role attached to the Lambda Function (ARN)"
}

variable "filename" {
  description = "Path to the zip file of the packed lambda function"
}

variable "s3_bucket" {
  description = "S3 bucket containing the lambda zip file"
}

variable "release" {}

resource "aws_s3_bucket_object" "lambda_s3_object" {
  bucket = "${var.s3_bucket}"
  key    = "${var.release}/${var.filename}.zip"
  source = "${path.root}/../../lambda/infra/placeholder/${var.filename}.zip"
  etag   = "${md5(file("${path.root}/../../lambda/infra/placeholder/${var.filename}.zip"))}"
}

resource "aws_lambda_function" "lambda" {
  function_name     = "${var.name}"
  role              = "${var.role}"
  handler           = "${var.handler}"
  runtime           = "${var.runtime}"
  s3_bucket         = "${var.s3_bucket}"
  s3_key            = "${aws_s3_bucket_object.lambda_s3_object.key}"
  s3_object_version = "${aws_s3_bucket_object.lambda_s3_object.version_id}"

  timeout          = "30"
  source_code_hash = "${base64sha256(file("${path.root}/../../lambda/infra/placeholder/${var.filename}.zip"))}"
}

output "name" {
  value = "${aws_lambda_function.lambda.function_name}"
}

output "arn" {
  value = "${aws_lambda_function.lambda.arn}"
}
