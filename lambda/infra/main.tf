resource "aws_dynamodb_table" "waterbill" {
  name           = "${var.release}_subscribers"
  read_capacity  = 10
  write_capacity = 10
  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "email"
    type = "S"
  }

  global_secondary_index {
    name               = "emailIndex"
    hash_key           = "email"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["id"]
  }
}

resource "aws_iam_role" "lambda_api_dynamo" {
  name               = "${var.release}_lambda_api_dynamo"
  assume_role_policy = "${file("${path.module}/role.json")}"
}

resource "aws_iam_role_policy" "lambda_dynamo_policy" {
  name = "${var.release}-lambda-dynamo-policy"
  role = "${aws_iam_role.lambda_api_dynamo.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# Now, we need an API to expose those functions publicly
resource "aws_api_gateway_rest_api" "waterbill_api" {
  name = "WaterBill API v${var.release}"
}

# The API requires at least one "endpoint", or "resource" in AWS terminology.
# The endpoint created here is: /${var.release}_waterbill e.g. v0_1_waterbill
resource "aws_api_gateway_resource" "waterbill_api_res" {
  rest_api_id = "${aws_api_gateway_rest_api.waterbill_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.waterbill_api.root_resource_id}"
  path_part   = "${var.release}_waterbill"
}

# Until now, the resource created could not respond to anything. We must set up
# a HTTP method (or verb) for that!
# This is the code for method GET /${var.release}_waterbill, that will talk to the first lambda
module "waterbill_get" {
  source                = "./api-gateway"
  method                = "GET"
  lambda                = "waterbill"
  lambda_role_arn       = "${aws_iam_role.lambda_api_dynamo.arn}"
  lambda_handler        = "WaterBill::WaterBill.Handler::Get"
  s3_bucket             = "waterbill-deploy"
  region                = "${var.region}"
  account_id            = "${var.account_id}"
  release               = "${var.release}"
  gateway_api_rest_id   = "${aws_api_gateway_rest_api.waterbill_api.id}"
  gateway_resource_id   = "${aws_api_gateway_resource.waterbill_api_res.id}"
  gateway_resource_path = "${aws_api_gateway_resource.waterbill_api_res.path}"

  integration_request = <<EOF1
  {
    "Id" : "$input.params('id')" 
  }
  EOF1

  response_model = <<EOF
  {
    "type": "object"
  }
  EOF

  passthrough = "WHEN_NO_MATCH"

  integration_response_model = ""
  method_request_parameters  = "${map("method.request.querystring.id", "true")}"
}

# # Here is a second lambda function that will run the lambda handler code
# module "lambda_post" {
#   source            = "./lambda"
#   name              = "${var.release}_waterbill_lambda_post"
#   runtime           = "dotnetcore1.0"
#   role              = "${aws_iam_role.lambda_api_dynamo.arn}"
#   s3_bucket         = "waterbill-deploy"
#   s3_key            = "${aws_s3_bucket_object.waterbill_s3_object.key}"
#   s3_object_version = "${aws_s3_bucket_object.waterbill_s3_object.version_id}"
#   filename          = "${path.root}/../../lambda/infra/placeholder/waterbill.zip"
#   handler           = "WaterBill::WaterBill.Handler::Post"
# }


# # API gateway for a post
# module "waterbill_post" {
#  depends_on             = ["module.lambda_get"]
#   source                = "./api-gateway"
#   method                = "POST"
#   lambda                = "${module.lambda_post.name}"
#   lambda_arn            = "${module.lambda_post.arn}"
#   region                = "${var.region}"
#   account_id            = "${var.account_id}"
#   release               = "${var.release}"
#   gateway_api_rest_id   = "${aws_api_gateway_rest_api.waterbill_api.id}"
#   gateway_resource_id   = "${aws_api_gateway_resource.waterbill_api_res.id}"
#   gateway_resource_path = "${aws_api_gateway_resource.waterbill_api_res.path}"
#   integration_request   = ""


#   response_model = <<EOF
#   {
#     "type": "object"
#   }
#   EOF


#   passthrough               = "WHEN_NO_MATCH"
#   integraion_response_model = ""
#   method_request_parameters = {}
# }

