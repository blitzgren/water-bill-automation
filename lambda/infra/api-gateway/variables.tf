variable "method" {
  description = "The HTTP method"
  default     = "GET"
}

variable "lambda" {
  description = "Name of the lambda function"
}

variable "lambda_role_arn" {
  description = "The lambda role arn identifier"
}

variable "lambda_handler" {
  description = "Handler code path for the lambda function"
}

variable "s3_bucket" {}

variable "region" {
  description = "The AWS region, e.g., us-east-2"
}

variable "account_id" {
  description = "The AWS account ID"
}

variable "integration_request" {}
variable "response_model" {}
variable "passthrough" {}
variable "integration_response_model" {}

variable "method_request_parameters" {
  type = "map"
}

variable "release" {
  default = "local"
}

variable "gateway_api_rest_id" {}
variable "gateway_resource_id" {}
variable "gateway_resource_path" {}
