module "lambda" {
  source  = "../lambda"
  name    = "${var.release}_${var.lambda}"
  runtime = "dotnetcore1.0"

  #   role              = "${aws_iam_role.lambda_api_dynamo.arn}"
  role      = "${var.lambda_role_arn}"
  s3_bucket = "${var.s3_bucket}"
  filename  = "${var.lambda}"
  handler   = "${var.lambda_handler}"
  release   = "${var.release}"
}

# Example: request for GET /waterbill
resource "aws_api_gateway_method" "request_method" {
  rest_api_id        = "${var.gateway_api_rest_id}"
  resource_id        = "${var.gateway_resource_id}"
  http_method        = "${var.method}"
  authorization      = "NONE"
  request_parameters = "${var.method_request_parameters}"
}

# Example: GET /waterbill => POST lambda
resource "aws_api_gateway_integration" "request_method_integration" {
  depends_on  = ["module.lambda"]
  rest_api_id = "${var.gateway_api_rest_id}"
  resource_id = "${var.gateway_resource_id}"
  http_method = "${aws_api_gateway_method.request_method.http_method}"
  type        = "AWS"
  uri         = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/${module.lambda.arn}/invocations"

  # AWS lambdas can only be invoked with the POST method
  integration_http_method = "POST"

  request_templates = {
    "application/json" = "${var.integration_request}"
  }

  passthrough_behavior = "${var.passthrough}"
}

resource "aws_api_gateway_model" "waterbill_response_model" {
  rest_api_id  = "${var.gateway_api_rest_id}"
  name         = "${var.release}${var.method}"
  description  = "A waterbill api endpoint"
  content_type = "application/json"
  schema       = "${var.response_model}"
}

# lambda => response
resource "aws_api_gateway_method_response" "response_method" {
  rest_api_id = "${var.gateway_api_rest_id}"
  resource_id = "${var.gateway_resource_id}"
  http_method = "${aws_api_gateway_integration.request_method_integration.http_method}"
  status_code = "200"

  response_models = {
    "application/json" = "${aws_api_gateway_model.waterbill_response_model.name}"
  }
}

resource "aws_api_gateway_integration_response" "response_method_integration" {
  rest_api_id = "${var.gateway_api_rest_id}"
  resource_id = "${var.gateway_resource_id}"
  http_method = "${aws_api_gateway_method_response.response_method.http_method}"
  status_code = "${aws_api_gateway_method_response.response_method.status_code}"

  #   response_templates = {
  #     "application/json" = "${var.integration_response_model}"
  #   }
}

resource "aws_lambda_permission" "allow_api_gateway" {
  depends_on    = ["module.lambda"]
  function_name = "${module.lambda.name}"
  statement_id  = "AllowExecutionFromApiGateway"
  action        = "lambda:InvokeFunction"
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.region}:${var.account_id}:${var.gateway_api_rest_id}/*/${var.method}${var.gateway_resource_path}"
}

# # We can deploy the API now! (i.e. make it publicly available)
resource "aws_api_gateway_deployment" "waterbill_api_deployment" {
  rest_api_id = "${var.gateway_api_rest_id}"
  stage_name  = "release"
  description = "Deploy: WaterBill GET and POST"

  depends_on = [
    "aws_api_gateway_method.request_method",
    "aws_api_gateway_method_response.response_method",
    "aws_api_gateway_integration.request_method_integration",
  ] # https://github.com/hashicorp/terraform/issues/7486
}
