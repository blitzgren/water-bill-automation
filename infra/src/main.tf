data "terraform_remote_state" "waterbill" {
  backend = "s3"

  config {
    bucket  = "waterbill-deploy"
    key     = "${var.release}/terraform.tfstate"
    region  = "${var.region}"
    profile = "${var.profile}"
  }
}

provider "aws" {
  region  = "${var.region}"
  profile = "${var.profile}"
}

module "waterbill" {
  source     = "../../lambda/infra"
  region     = "${var.region}"
  profile    = "${var.profile}"
  account_id = "${var.account_id}"
  release    = "${var.release}"
}
